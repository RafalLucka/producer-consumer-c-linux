#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <err.h>
#include <fcntl.h>
#include <time.h>
#include <stdint.h>
#define INPUT_SIZE  1024

/*description
-Semaphores are used as synchronization method only to pull PIDs from file
-Subprocesses need PIDs to communicate with each other about ending work
 and allowing other subprocess to start working
-Subprocesses communicate with each other by sending signal SIGCONT (kill(process_pid_,SIGCONT))
*/

int pid[3];//holds PID's of child processes
int pipe1[2], pipe2[2];//descriptor of pipe file, 0 - read, 1 - save
//-----flags-------------------------//
int dataFlag = 1;//data correctness flag
int endFlag = 1;//process continuation flag
int haltFlag = 0;
int synchronizationLock = 1;
int readSignalLock = 1;
//-----data and pointers--//
int result = 0;
int queue;
char data[INPUT_SIZE];
FILE *PIDs_file;
FILE* fifoPointer;


struct mymsgbuf {
	long type;          
	int  request; 
    char data[INPUT_SIZE];
};
struct mymsgbuf msg[4];

int nextProcess(){
	for(int i = 0 ; i < 3 ; i++)
		if(pid[i] == getpid() && i != 2) return i + 1;
	return 0;
}
int previousProcess(){
	for(int i = 0 ; i < 3 ; i++)
		if(pid[i] == getpid() && i != 0) return i - 1;
	return 2;
}

int open_queue() {
	key_t key = ftok(".", 'm');
	if((queue = msgget( key, IPC_CREAT | 0660 )) == -1)
		return(-1);
	return(queue);
}

int remove_queue(){
	if( msgctl(queue, IPC_RMID, 0) == -1)
		return(-1);
	return(0);
}

int read_message( int qid, long type, struct mymsgbuf *qbuf ){
	int result, length;
	length = sizeof(struct mymsgbuf) - sizeof(long);        
	if((result = msgrcv( qid, qbuf, length, type,  0)) == -1)
		return(-1);   
	return(result);
}

int send_message( int qid, struct mymsgbuf *qbuf ){
	int result, length;
	length = sizeof(struct mymsgbuf) - sizeof(long);
	if((result = msgsnd( qid, qbuf, length, 0)) == -1)
          return(-1);
	return(result);
}

void write_queue(int arg){
	msg[arg].type = 1;
	msg[arg].request = 1;
	strcpy(msg[arg].data, data);
	if(send_message(queue, &msg[arg]) == -1){
		perror("Wysylanie");
		exit(1);
	}
}
void read_queue(int arg){
	msg[arg].type = 1;
	msg[arg].request = 1;
	read_message(queue, msg[arg].type, &msg[arg]);
	strcpy(data, msg[arg].data);	
} 

void waitForUnlock(int mode){
	if(mode) while(synchronizationLock){;}
	else{
		readSignalLock = 1;
		while(readSignalLock){;}
	} 
}

void unlockProcess(int mode){//mode 1-data synchronization 2-signal read synchronization
	if (mode == 1){
		synchronizationLock = 1;//self lock
		kill(pid[nextProcess()], SIGCONT);
		//when process recieves SIGCONT, set synchronizationLock to 0
	}else if(mode == 0){
		readSignalLock = 1;//self lock
		kill(pid[nextProcess()],SIGRTMAX);
	}else{
		synchronizationLock = 1;//zablokowanie samego siebie
		kill(pid[0], SIGCONT);
	}
}

void exportPIDs(){//saving PIDs to PID_info.txt	
	if(PIDs_file = fopen("PID_info.txt","w")){
		for(int i = 0; i < 3; i++) fprintf(PIDs_file, "%d ", pid[i]);
		fclose(PIDs_file);
	}else fprintf(stderr, "PIDs holding file was not created!\n");	
	unlockProcess(1);
}

void getPIDs(){//pobranie pidów do tablicy pid[]
	waitForUnlock(1);
		if(PIDs_file = fopen("PID_info.txt","r")){
			for(int i = 0; i < 3; i++) fscanf(PIDs_file, "%d", &pid[i]);
			fclose(PIDs_file);
		}else fprintf(stderr, "Warning! File PID_info.txt not found");
	unlockProcess(1);
}

void cleanUp(){
	remove_queue();
	system("rm fifo");
	system("rm PID_info.txt");
	kill(getpid(), SIGKILL);
}

void checkSignal(){
	int recievedCommand;
	if(fifoPointer = fopen("fifo","r")){
		fscanf(fifoPointer, "%d", &recievedCommand);
		//fprintf(stderr,"%d reading %d\n",getpid(),recievedCommand);
		fclose(fifoPointer);
		//fprintf(stderr,"Reading has ended, closing...\n");
	}else fprintf(stderr,"Could not open fifo file\n");
	if(recievedCommand == 0) haltFlag = 1; //pausing
	if(recievedCommand == 1) haltFlag = 0; //resuming
    kill(pid[previousProcess()], SIGRTMAX);
}

void writeSignal(int command){
	kill(pid[nextProcess()], SIGTSTP);
	for(int i = 0; i <= 1; i++){ 
		if(fifoPointer=fopen("fifo","w")){
			fprintf(fifoPointer, "%d", command);
			//fprintf(stderr,"%d writing %d\n",getpid(),command);
			fclose(fifoPointer);
			//fprintf(stderr,"%d closed after writing\n",getpid());
			if(i == 0){
				waitForUnlock(0);
				if(nextProcess() != 2) kill(pid[nextProcess()+1], SIGTSTP);//inform last process
				else kill(pid[0], SIGTSTP);
			}
		}else fprintf(stderr, "Could not open fifo file");
        
	}
}

void mainHandler(int signal){
	if (signal == SIGINT){
		fprintf(stderr, "\n Main process: %d, ending work\n", getpid());
			for(int i = 0; i <= 2; i++) kill(pid[i], SIGKILL);				
			endFlag = 0;
	}
	if(signal == SIGUSR1 && !haltFlag){ 
		fprintf(stderr, "Pausing work\n");
		haltFlag = 1;
	}
	if(signal == SIGUSR2 && haltFlag){
		fprintf(stderr, "Resuming work\n");
		haltFlag = 0;
	} 
}

void childHandler(int signal){
	kill(getppid(), signal);
	if(signal == SIGCONT)	synchronizationLock = 0;
	if(signal == SIGRTMAX) 	readSignalLock = 0;
	if(signal == SIGUSR1)	{writeSignal(0); haltFlag = 1;}
	if(signal == SIGUSR2)	{writeSignal(1); haltFlag = 0;}
	if(signal == SIGTSTP) checkSignal();
	//all printing is handled by main process, which prevents unwanted, multiple displays	
}

void check_if_correct(){
    dataFlag = 1;
    for(int i = 0; i < strlen(data) - 1; i++) {
    if((int)data[i] < 48 || (int)data[i] > 57) {
       if((int)data[i] != 43) dataFlag = 0;
        }
	}
	if(dataFlag == 0)
	fprintf(stderr, "Wrong data format!");
}

int pot(int a, int b) { //exponentiation
    if (b == 0) return 0;
    int c = 1;
    while(b > 0) {
    c = c * a;
    b -= 1;
    }
    return c;
}

void calculate_result(){
    if(dataFlag == 0) result = 0;
    else{
    int x = 0; //addition variable
    int count = 0;
    for(int i = 0; i < strlen(data); i++) {
        while((int)data[i] != 43 || data[i] != '\0') count += 1; //increment "c" until you arrive to '+' or '\0'
        if((int)data[i] == 43 || data[i] == '\0') {
        while(count > 0) {
        printf(" %d\n", (((int)data[i - (count + 1)] - 48) * pot(10, count)));
        x += (((int)data[i - (count + 1)] - 48) * pot(10, count));
        count -= 1; 
            }
        }
    }
    result = x;
    }
    //dane są w globalnej tablicy dane[]
	//tutaj liczysz sobie wynik i zapisujesz go do globalnego int result
}

void p1(){
	getPIDs();
	while(1) {
		waitForUnlock(1);
			fprintf(stderr, "\nInput data: \n");
			fgets(data,INPUT_SIZE,stdin);
			data[strlen(data) - 1]='\0';
			while(haltFlag){;} 
			fprintf(stderr, "[P1:%d] Data:%s \n", getpid(), data);
			write_queue(0);
		unlockProcess(1);		
	}
}

void p2(){
	getPIDs();
	while(1) {
		waitForUnlock(1);
			while(haltFlag){;}
			read_queue(1);
			fprintf(stderr, "[P2:%d] recieved: %s\n", getpid(), data);
			check_if_correct();
			write_queue(2);
		if(dataFlag) unlockProcess(1);
		else unlockProcess(2);
	}
}

void p3(){
	getPIDs();
	while(1) {
		waitForUnlock(1);
			while(haltFlag){;}
			read_queue(3);
			fprintf(stderr, "[P3:%d] recieved: %s\n", getpid(), data);
			calculate_result();//musisz zaimplementować tą funckję
			fprintf(stderr, "[P3:%d] result: %d\n", getpid(), result);
		unlockProcess(1);
	}
}

void createProcesses(){
	for (int i=0;i<=2;i++){
		if((pid[i]=fork())==0){//<--creating new child process
			//---------display information about child process---------//
			fprintf(stderr,"Process %d PID:%d PPID:%d\n", i+1, getpid(), getppid());
			//---------handling all signals in subprocesses----------//
			for(int k = 1; k <= 64; k++) signal(k, childHandler);		
			//---------every process calls own function----------------//	
			switch (i){
				case 0:	p1(); break;
				case 1:	p2(); break;
				case 2:	p3(); break;
				default: fprintf(stderr,"Something went terribly wrong!");
			}
		}
	}
}

int main() {
	fprintf(stderr,"Main process PID:%d PPID:%d\n", getpid(), getppid());
	for(int k = 1; k <= 64; k++) signal(k, mainHandler);
	open_queue();
	umask(0);
	mkfifo("fifo", 0666);
	createProcesses();
	sleep(1);
	exportPIDs();
	while (endFlag) pause();
	cleanUp();
	return 0;
}
