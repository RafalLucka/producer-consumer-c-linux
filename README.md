# Producer-Consumer-C-Linux

This program in C uses message queues to realize producer-consumer type of program using messages queues, FIFO queues and signals. Whole project written in Linux Mint OS

How does it work?:
Process1 - reads data (from single verse) and sends them in unchanged form to Process2. Verses are expressions in form: number1+number2+...numbern
Process2 - gets data from Process1 and verifies its correctness (if they are in desired type). If yes, sends them to Process3
Process3 - gets data from Process2, calculates expression and puts it in a standard output stream.

Every process can be manipulated with signals sent by operator, which can end process, halt it, or resume it.